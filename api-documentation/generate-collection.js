require('dotenv').config();
const {Collection, Item, Header} = require('postman-collection');
const fs = require('fs');

const {AUTH_TOKEN} = process.env;

const postmanCollection = new Collection({
    info: {
      name: "Dokumentasi API Challenge Chapter-5",
    },
    item: [],
  });
  
  //set header
const rawHeaderString = `Authorization:${AUTH_TOKEN}\nContent-Type:application/json\ncache-control:no-cache\n`;
  
const rawHeader = Header.parse(rawHeaderString);
  
const requestHeader = rawHeader.map((h) => new Header(h));  

// Login
const authLogin = new Item({
    name: "Auth Login",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/auth/login",
        method: "POST",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                username: "sekarmk",
                password: "sekarmk03"
            }),
        },
        auth: null,
    },
});

postmanCollection.items.add(authLogin);

// User Game
const allUserGame = new Item({
    name: "All User Game",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-games/",
        method: "GET",
        auth: null,
    },
});
const detailUserGame = new Item({
    name: "Show Detail User Game",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-games/1",
        method: "GET",
        auth: null,
    },
});
const addUserGame = new Item({
    name: "Add New User Game",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-games/",
        method: "POST",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                username: "sabrina",
                password: "secret789"
            }),
        },
        auth: null,
    },
});
const updateUserGame = new Item({
    name: "Update User Game",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-games/1",
        method: "PUT",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                username: "sekarmk",
                password: "sekar123"
            }),
        },
        auth: null,
    },
})
const deleteUserGame = new Item({
    name: "Delete User Game",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-games/3",
        method: "DELETE",
        auth: null,
    },
});

postmanCollection.items.add(allUserGame);
postmanCollection.items.add(detailUserGame);
postmanCollection.items.add(addUserGame);
postmanCollection.items.add(updateUserGame);
postmanCollection.items.add(deleteUserGame);

// User Game Biodata
const allUserBiodata = new Item({
    name: "All User Biodata",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-biodata/",
        method: "GET",
        auth: null,
    },
});
const detailUserBiodata = new Item({
    name: "Show Detail User Biodata",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-biodata/1",
        method: "GET",
        auth: null,
    },
});
const addUserBiodata = new Item({
    name: "Add New User Biodata",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-biodata/",
        method: "POST",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                user_id: 3,
                fullname: "Sabrina Binar",
                email: "sabrina@gmail.com",
                telp: "089123456789",
                birthday: "2017-08-03",
                region: "Indonesia",
            }),
        },
        auth: null,
    },
});
const updateUserBiodata = new Item({
    name: "Update User Biodata",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-biodata/3",
        method: "PUT",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                user_id: 3,
                fullname: "Sabrina Binar Academy",
                email: "sabrina.binar@gmail.com",
                telp: "089123456789",
                birthday: "2017-08-03",
                region: "Indonesia",
            }),
        },
        auth: null,
    },
})
const deleteUserBiodata = new Item({
    name: "Delete User Biodata",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-biodata/3",
        method: "DELETE",
        auth: null,
    },
});

postmanCollection.items.add(allUserBiodata);
postmanCollection.items.add(detailUserBiodata);
postmanCollection.items.add(addUserBiodata);
postmanCollection.items.add(updateUserBiodata);
postmanCollection.items.add(deleteUserBiodata);

// User Game History
const allUserHistory = new Item({
    name: "All User History",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-histories/",
        method: "GET",
        auth: null,
    },
});
const detailUserHistory = new Item({
    name: "Show Detail User History",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-histories/1",
        method: "GET",
        auth: null,
    },
});
const addUserHistory = new Item({
    name: "Add New User History",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-histories/",
        method: "POST",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                user_id: 3,
                spend_time: 4000,
                result: "draw",
                score: 14000,
            }),
        },
        auth: null,
    },
});
const updateUserHistory = new Item({
    name: "Update User History",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-histories/3",
        method: "PUT",
        body: {
            mode: "raw",
            raw: JSON.stringify({
                user_id: 3,
                spend_time: 4500,
                result: "win",
                score: 14500,
            }),
        },
        auth: null,
    },
})
const deleteUserHistory = new Item({
    name: "Delete User History",
    request: {
        header: requestHeader,
        url: "http://localhost:3000/user-game-histories/3",
        method: "DELETE",
        auth: null,
    },
});

postmanCollection.items.add(allUserHistory);
postmanCollection.items.add(detailUserHistory);
postmanCollection.items.add(addUserHistory);
postmanCollection.items.add(updateUserHistory);
postmanCollection.items.add(deleteUserHistory);

// Convert data to JSON format
const collectionJSON = postmanCollection.toJSON();

// Write JSON file
fs.writeFile('./collection.json', JSON.stringify(collectionJSON, null, 2), (err) => {
    if (err) console.log(err);
    console.log('file saved!');
});
