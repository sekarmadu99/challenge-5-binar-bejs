const express = require('express');
const router = express.Router();
const controller = require('../controllers');

router.get('/', controller.userGame.index);
router.get('/:userGameId', controller.userGame.show);
router.post('/', controller.userGame.create);
router.put('/:userGameId', controller.userGame.update);
router.delete('/:userGameId', controller.userGame.delete);

module.exports = router;