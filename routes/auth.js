const express = require('express');
const router = express.Router();
const controller = require('../controllers');
const middleware = require('../helpers/middleware');

router.post('/login', controller.auth.login);

module.exports = router;