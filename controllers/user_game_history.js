const {user_game_history} = require('../models');

module.exports = {
    index: async (req, res, next) => {
        try {
            const userGameHistories = await user_game_history.findAll({raw: true});
            return res.status(200).json({
                status: true,
                message: 'Get all User Histories data successfully',
                data: userGameHistories
            })
        } catch (err) {
            next(err);
        }
    },
    show: async (req, res, next) => {
        try {
            const {historyId} = req.params;
            const userGameHistory = await user_game_history.findOne({where: {id: historyId}});
            if(!userGameHistory) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }
            return res.status(200).json({
                status: true,
                message: 'Get data User History successfully!',
                data: userGameHistory.get()
            });
        } catch (err) {
            next(err);
        }
    },
    create: async (req, res, next) => {
        try {
            const {user_id, spend_time, result, score} = req.body;

            const userGameHistory = await user_game_history.create({
                user_id,
                spend_time,
                result,
                score
            });

            return res.status(201).json({
                status: true,
                message: 'Create new User\'s History successfully!',
                data: userGameHistory
            });
        } catch (err) {
            next(err);
        }
    },
    update: async (req, res, next) => {
        try {
            const {historyId} = req.params;
            let {user_id, spend_time, result, score} = req.body;

            const userGameHistory = await user_game_history.findOne({where: {id: historyId}});
            if(!userGameHistory) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }

            if(!user_id) user_id = userGameHistory.user_id;
            if(!spend_time) spend_time = userGameHistory.spend_time;
            if(!result) result = userGameHistory.result;
            if(!score) score = userGameHistory.score;
            
            const updateUserHistory = await user_game_history.update({
                user_id,
                spend_time,
                result,
                score
            }, {
                where: {id: historyId}
            });

            const newData = await user_game_history.findOne({where: {id: historyId}});

            return res.status(200).json({
                status: true,
                message: 'Update User\'s History successfully!',
                data: newData.get()
            });
        } catch (err) {
            next(err);
        }
    },
    delete: async (req, res, next) => {
        try {
            const {historyId} = req.params;

            const userGameHistory = await user_game_history.findOne({where: {id: historyId}});
            if(!userGameHistory) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }

            const deleted = await user_game_history.destroy({
                where: {id: historyId}
            });

            const userGameHistories = await user_game_history.findAll({raw: true});

            return res.status(200).json({
                status: true,
                message: 'Delete User\'s History successfully!',
                data: userGameHistories
            });
        } catch (err) {
            next(err);
        }
    }
}