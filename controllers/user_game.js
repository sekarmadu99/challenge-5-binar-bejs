const {user_game} = require('../models');
const bcrypt = require('bcrypt');

module.exports = {
    index: async (req, res, next) => {
        try {
            const usersGameData = await user_game.findAll({raw: true});
            return res.status(200).json({
                status: true,
                message: 'Get all User Game data successfully',
                data: usersGameData
            })
        } catch (err) {
            next(err);
        }
    },
    show: async (req, res, next) => {
        try {
            const {userGameId} = req.params;
            const userGameData = await user_game.findOne({where: {id: userGameId}});
            if(!userGameData) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }
            return res.status(200).json({
                status: true,
                message: 'Get data User Game successfully!',
                data: userGameData.get()
            });
        } catch (err) {
            next(err);
        }
    },
    create: async (req, res, next) => {
        try {
            const {username, password} = req.body;
            
            const isExist = await user_game.findOne({where: {username: username}});
            if(isExist) {
                return res.status(409).json({
                    status: false,
                    message: 'Username already used!'
                });
            }

            const userGameData = await user_game.create({
                username,
                password: await bcrypt.hash(password, 10),
            });

            return res.status(201).json({
                status: true,
                message: 'Create new User Game successfully!',
                data: {
                    username: userGameData.username
                }
            });
        } catch (err) {
            next(err);
        }
    },
    update: async (req, res, next) => {
        try {
            const {userGameId} = req.params;
            let {username, password} = req.body;

            const userGameData = await user_game.findOne({where: {id: userGameId}});
            if(!userGameData) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }

            if(!username) username = userGameData.username;
            if(!password) password = userGameData.password;

            const updateUserData = await user_game.update({
                username,
                password: await bcrypt.hash(password, 10)
            }, {
                where: {id: userGameId}
            });

            const newData = await user_game.findOne({where: {id: userGameId}});

            return res.status(200).json({
                status: true,
                message: 'Update User Game data successfully!',
                data: newData.get()
            });
        } catch (err) {
            next(err);
        }
    },
    delete: async (req, res, next) => {
        try {
            const {userGameId} = req.params;

            const userGameData = await user_game.findOne({where: {id: userGameId}});
            if(!userGameData) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }

            const deleted = await user_game.destroy({
                where: {id: userGameId}
            });

            const usersGameData = await user_game.findAll({raw: true});

            return res.status(200).json({
                status: true,
                message: 'Delete User Game data successfully!',
                data: usersGameData
            });
        } catch (err) {
            next(err);
        }
    }
}