const {user_game_biodata} = require('../models');

module.exports = {
    index: async (req, res, next) => {
        try {
            const usersGameBiodata = await user_game_biodata.findAll({raw: true});
            return res.status(200).json({
                status: true,
                message: 'Get all User Biodata data successfully',
                data: usersGameBiodata
            })
        } catch (err) {
            next(err);
        }
    },
    show: async (req, res, next) => {
        try {
            const {userBiodataId} = req.params;
            const userGameBiodata = await user_game_biodata.findOne({where: {id: userBiodataId}});
            if(!userGameBiodata) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }
            return res.status(200).json({
                status: true,
                message: 'Get data User Game successfully!',
                data: userGameBiodata.get()
            });
        } catch (err) {
            next(err);
        }
    },
    create: async (req, res, next) => {
        try {
            const {user_id, fullname, email, telp, birthday, region} = req.body;
            
            const isExist = await user_game_biodata.findOne({where: {user_id: user_id}});
            if(isExist) {
                return res.status(409).json({
                    status: false,
                    message: 'User\'s Biodata already exist!'
                });
            }

            const userGameBiodata = await user_game_biodata.create({
                user_id,
                fullname,
                email,
                telp,
                birthday,
                region
            });

            return res.status(201).json({
                status: true,
                message: 'Create new User\'s Biodata successfully!',
                data: userGameBiodata
            });
        } catch (err) {
            next(err);
        }
    },
    update: async (req, res, next) => {
        try {
            const {userBiodataId} = req.params;
            let {user_id, fullname, email, telp, birthday, region} = req.body;

            const userGameBiodata = await user_game_biodata.findOne({where: {id: userBiodataId}});
            if(!userGameBiodata) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }

            if(!user_id) user_id = userGameBiodata.user_id;
            if(!fullname) fullname = userGameBiodata.fullname;
            if(!email) email = userGameBiodata.email;
            if(!telp) telp = userGameBiodata.telp;
            if(!birthday) birthday = userGameBiodata.birthday;
            if(!region) region = userGameBiodata.region;

            const updateUserBiodata = await user_game_biodata.update({
                user_id,
                fullname,
                email,
                telp,
                birthday,
                region
            }, {
                where: {id: userBiodataId}
            });

            const newData = await user_game_biodata.findOne({where: {id: userBiodataId}});

            return res.status(200).json({
                status: true,
                message: 'Update User\'s Biodata successfully!',
                data: newData.get()
            });
        } catch (err) {
            next(err);
        }
    },
    delete: async (req, res, next) => {
        try {
            const {userBiodataId} = req.params;

            const userGameBiodata = await user_game_biodata.findOne({where: {id: userBiodataId}});
            if(!userGameBiodata) {
                return res.status(400).json({
                    status: false,
                    message: 'Data not found!',
                    data: null
                });
            }

            const deleted = await user_game_biodata.destroy({
                where: {id: userBiodataId}
            });

            const usersGameBiodata = await user_game_biodata.findAll({raw: true});

            return res.status(200).json({
                status: true,
                message: 'Delete User\'s Biodata successfully!',
                data: usersGameBiodata
            });
        } catch (err) {
            next(err);
        }
    }
}